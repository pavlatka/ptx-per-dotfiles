#qutebrowser
cp ~/Library/Preferences/qutebrowser/autoconfig.yml ./qutebrowser/autoconfig.yml
cp ~/.qutebrowser/quickmarks ./qutebrowser/quickmarks
cp ~/.qutebrowser/bookmarks/urls ./qutebrowser/urls
cp ~/.tmux.conf ./tmux.conf
cp ~/.vimrc ./vimrc
cp ~/.gitconfig ./gitconfig
cp ~/.gitignore ./gitignore
cp ~/.config/zsh/.zshrc ./zshrc
cp ~/.vim/UltiSnips/php.snippets ./UltiSnips/
cp ~/.vim/UltiSnips/_.snippets ./UltiSnips/
