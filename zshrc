zmodload zsh/zprof

# Path to your oh-my-zsh installation.
export ZSH=~/.oh-my-zsh

autoload -U colors && colors
PS1="%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[red]%}]%{$reset_color%}$%b "

# Theme + basic:
ZSH_THEME="sunrise"

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.cache/zsh/history
HIST_STAMPS="dd/mm/yyyy"

# User configuration
export LANG=en_US.UTF-8

bindkey -v
export KEYTIMEOUT=1

plugins=(git)

source $ZSH/oh-my-zsh.sh

# Preferred editor
export EDITOR="/usr/local/bin/vim"
export USE_EDITOR=$EDITOR
export VISUAL=$EDITOR
export PF_COL2=4

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# Aliases:
alias python="python3"
alias vim="/usr/local/bin/vim"
alias wf="/Users/tpavlatka/codebase/personal/wf3/wf3"
alias mamba="docker pull wayfair/mamba:latest && docker run -it wayfair/mamba:latest"
alias ntest="./includes/sdk/composer-packages/bin/phpunit -c ./applications/wms/nexus/phpunit.xml.dist --testsuite=nexusSpoCrossDock"
alias ncover="./includes/sdk/composer-packages/bin/phpunit -c ./applications/wms/nexus/phpunit.xml.dist --testsuite=nexusTransship --coverage-crap4j ~/codebase/Wayfair/xdebug/data/report.xml && php ~/codebase/Wayfair/xdebug/index.php && cd ~/codebase/Wayfair/sync/php"
alias drclean="docker rm -f $(docker ps -aq)"
alias nstan="cd ~/codebase/Wayfair/sync/wms-nexus && ./vendor/bin/phpstan analyze -c ~/codebase/Wayfair/nexus_phpstan.neon -vvv ./src/Application_Services/SpoCrossDock ./src/Domains/SpoCrossDock ./src/HTTP_Services/Secure/SpoCrossDock ./tests/Unit/Application_Services/SpoCrossDock ./tests/Unit/Domains/SpoCrossDock ./tests/Unit/HTTP_Services/Secure/SpoCrossDock"

DISABLE_UNTRACKED_FILES_DIRTY="true"

# Path:
export PATH="/Users/tpavlatka/bin:$PATH"
export PATH="/Users/tpavlatka/go/bin:$PATH"
export PATH="/usr/local/Cellar/ufraw/0.22_3/bin:$PATH"
export PATH="/Users/tpavlatka/Downloads/Bento4-SDK-1-6-0-637.universal-apple-macosx/bin/:$PATH"

. ~/bin/z.sh
export PATH="/usr/local/sbin:$PATH"
