call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'udalov/kotlin-vim'
Plug 'junegunn/fzf.vim'
Plug 'rking/ag.vim'
Plug 'mikelue/vim-maven-plugin'
Plug 'tpope/vim-abolish'
Plug 'yuratomo/neon.vim'
Plug 'junegunn/vim-easy-align'
Plug 'ntpeters/vim-better-whitespace'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'mbbill/undotree'
Plug 'gruvbox-community/gruvbox'
Plug 'tpope/vim-fugitive'
Plug 'ervandew/supertab'
Plug 'SirVer/ultisnips'
Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'airblade/vim-gitgutter'
Plug 'vimwiki/vimwiki'
Plug 'justinmk/vim-sneak'
Plug 'christoomey/vim-sort-motion'

" Add plugins to &runtimepath
call plug#end()

if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

" --------
" SETTINGS
" --------
syntax on
set exrc
set cmdheight=2
set background=dark
set encoding=utf8
set tabstop=2 shiftwidth=2 expandtab
set nocompatible              " be iMproved, required
set signcolumn=yes
set relativenumber
set number
set nowrap
set smartcase
set noerrorbells
set incsearch
set nohlsearch
set nobackup
set directory=~/.vim/swapfiles//
set spell spelllang=en_us,de_de
set termguicolors
set colorcolumn=99
set scrolloff=8
set updatetime=50
set shortmess+=c

colorscheme gruvbox
let g:grubox_contrast_dark="hard"

" --------
" SHORTCUTS
" --------

:inoremap jk <esc>
:inoremap <esc> <nop>
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

:command WQ wq
:command Wq wq
:command W w
:command Q q
:command Qa qa

" Speed up work
nnoremap <C-e> 5<C-e>
nnoremap <C-y> 5<C-y>

" Tabs remapping
nnoremap tn :tabnew<space>
nnoremap tk :tabnext<cr>
nnoremap tj :tabprev<cr>
nnoremap th :tabfirst<cr>
nnoremap tl :tablast<cr>

nnoremap nt :NERDTreeToggle<cr>

:autocmd InsertEnter * set cul
:autocmd InsertLeave * set nocul
autocmd BufWritePre * StripWhitespace

xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" ------
" LEADER
" ------

let mapleader = ' '

map <Leader>pcd :put =strftime('%a %d/%m')<cr>

map <Leader>bf :Buffers<cr>
map <Leader>pf :GFiles<cr>
map <Leader>ff :Files<cr>
map <Leader>rf :NERDTreeFind<cr>

nnoremap <leader>ghw :h <C-R>=expand("<cword>")<CR><CR>
nnoremap <leader>pfd :CocList diagnostics<CR>


" Set UltiSnips triggers
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

map f <Plug>Sneak_s
map F <Plug>Sneak_S
map t <Plug>Sneak_t
map T <Plug>Sneak_T

" VimWiki
let g:vimwiki_list = [{'path': '$HOME/codebase/personal/ptx_wiki', 'syntax' : 'markdown', 'ext' : '.md', 'auto_diary_index': 1}]

let g:ackprg = 'ag --nogroup --nocolor --column | fzf'

let g:NERDTreeWinSize = 40
let g:NERDTreeNodeDelimiter = "\u00a0"

let g:sneak#label = 1

" Maven
let g:maven_keymaps = 1
nmap <leader>mrt <Plug>MavenRunUnittest
nmap <leader>mrat <Plug>MavenRunUnittestAll
nmap <leader>mtt <Plug>MavenSwitchUnittestFile

let g:fzf_layout = { 'down': '20%' }
let $FZF_DEFAULT_OPTS='--no-reverse'

" --------------------------
" COC - Completion settings
" --------------------------
" Use tab for trigger completion with characters ahead and navigate.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocActionAsync('doHover')
  endif
endfunction

nmap <leader>gd <Plug>(coc-definition)
nmap <leader>gi <Plug>(coc-implementation)
nmap <leader>gr <Plug>(coc-references)

" Because I do not know how to type correctly :)
iabbr ture true
iabbr fales false
iabbr fuction function
iabbr funciton function
iabbr fucntion function
iabbr functoin function
iabbr retrun return
iabbr reutrn return
iabbr 4this $this
iabbr 4This $this
iabbr RBD REMOVE_BEFORE_DEPLOY

iabbr stirng string
iabbr YOu You
iabbr YOur Your
iabbr 2param @param
iabbr 2var @var

iabbr tmr tomorrow
iabbr tda today
iabbr ystd yesterday
iabbr henry Henry
iabbr george George
iabbr ladia Ladia
iabbr lisa Lisa

iabbr jsut just
iabbr taht that
iabbr pls please
iabbr Pls Please
iabbr Rigth Right
iabbr WTRAT ## What to remember about today

set secure
