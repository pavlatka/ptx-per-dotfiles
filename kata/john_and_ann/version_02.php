<?php

function john(int $day) : array {
  $katas = [];
  for ($i = 0; $i < $day; $i++) {
    $katas[] = johnDay($i);
  }

  return $katas;
}

function ann(int $day) : array {
  $katas = [];
  for ($i = 0; $i < $day; $i++) {
    $katas[] = annDay($i);
  }

  return $katas;
}

function johnDay(int $day) : int {
  static $repo = [];

  if (!array_key_exists($day, $repo)) {
    $katas = $day <= 0 ? 0 : ($day - annDay(johnDay($day - 1)));

    $repo[$day] = $katas > 0 ? $katas : 0;
  }

  return $repo[$day];
}

function annDay(int $day) : int {
  static $repo = [];

  if (!array_key_exists($day, $repo)) {
    $katas = $day <= 0 ? 1 : ($day - johnDay(annDay($day - 1)));

    $repo[$day] = $katas > 0 ? $katas : 0;
  }

  return $repo[$day];
}

function sumJohn(int $day) : int {
  return array_sum(john($day));
}

function sumAnn(int $day) : int {
  return array_sum(ann($day));
}

$part = $argv[1] ?? 'help';
$day  = $argv[2] ?? 3;

echo 'JOHN and ANN KATAS' . PHP_EOL;
echo '==================' . PHP_EOL;

if (!is_numeric($day)) {
  echo '- Day must be a number!' . PHP_EOL;
  exit;
}

$day = (int)$day;

echo '- AVAILABLE PARTS: john, ann, sumJohn, sumAnn' . PHP_EOL;
echo '- PART: ' . $part . PHP_EOL;
echo '- DAY: ' . $day . PHP_EOL;

if (function_exists($part)) {
  echo '<pre>';
  var_dump($part($day));
  echo '</pre>';
  exit;
}
