<?php
// The solution I liked, but not my own

function john($n)
{
  return getKata($n)[1];
}

function ann($n)
{
  return getKata($n)[0];
}

function sumJohn($n)
{
    return array_sum(getKata($n)[1]);
}

function sumAnn($n)
{
    return array_sum(getKata($n)[0]);
}

function getKata($n)
{
  $a[0] = 1;
  $j[0] = 0;

  for ($i = 1; $i < $n; ++$i) {
    $j[$i] = $i - $a[$j[$i - 1]];
    $a[$i] = $i - $j[$a[$i - 1]];
  }

  return [$a, $j];
}

