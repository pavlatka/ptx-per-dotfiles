<?php

function bouncingBall(float $height, float $boucing, float $mother) : int {
  if ($height <= 0 || $boucing <= 0 || $boucing >= 1 || $mother >= $height) {
    return -1;
  }

  $seen = 1;

  $height *= $boucing;
  while ($height >= $mother) {
    $seen += 2;

    $height *= $boucing;
  }

  return $seen;
}

$height  = 30.0;
$boucing = 0.66;
$mother  = 1.5;

echo boucingBall($height, $boucing, $mother);

